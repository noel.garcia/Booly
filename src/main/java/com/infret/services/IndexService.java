package com.infret.services;

import com.infret.utils.LoadFile;

import java.io.File;
import java.util.*;

/**
 * Created by noelgarcia on 04/10/15.
 */
public class IndexService {
    private String STOP_WORDS_FILE = "src/main/resources/indexFiles/stopWords.txt";

    private LoadFile loadFile = new LoadFile();
    private Hashtable<String,LinkedList<String>> index = new Hashtable<String, LinkedList<String>>();
    private ArrayList<String> stopWords = new ArrayList<String>();

    public IndexService() {
        start();
    }

    /**
     * Para pruebas
     */
    public void start(){
        addStopWords();
        System.out.println(stopWords);

        reindex();
    }

    public void reindex() {
        index.clear();

        ArrayList<String> filesFromDir = loadFile.getFilesFromDir("src/main/resources/public/views/indexer/");
        System.out.println(filesFromDir.toString());

        for (String file : filesFromDir){
            addIndex(file);
        }
    }

    /**
     * Método que agrega una entrada al Indice (String Key, LinkedList[Keywords]).
     * Si el archivo ya se había indexado, se reemplaza.
     * @param file es el archivo a indexar.
     */
    public void addIndex(String file){
        File archivo = new File(file);
        String fileName = "";
        String validWords = "";
        LinkedList<String> keyWords = new LinkedList<String>();

        if (archivo.exists()){
            fileName = archivo.getName();
            String x = loadFile.getFile(file);
            keyWords = getKeyWords(x);
            for (String keyword : keyWords){
                if (index.containsKey(keyword)){
                    if (!index.get(keyword).contains(fileName))
                        index.get(keyword).add(fileName);
                } else {
                    LinkedList<String> docs = new LinkedList<String>();
                    docs.add(fileName);
                    index.put(keyword, docs);
                }
            }
            System.out.println("Indexado: "+fileName);
        }
    }

    public Hashtable<String, LinkedList<String>> getIndex() {
        return index;
    }

    public boolean removeIndex(String file){
        if (index.remove(file).equals(null))
            return false;
        return true;

    }

    /*
    TODO: Las stop words deben venir como parametro, para en el interfaz web
          agregar un apartado para "agregar stopwords"
     */
    public boolean addStopWord(String stopWord){
        String arrelgo[];

        if (stopWord.equals(null))
            return false;

        arrelgo = stopWord.toLowerCase().split(" ");
        for (int i = 0; i < arrelgo.length; i++){
            if (!stopWords.contains(arrelgo[i]))
                stopWords.add(arrelgo[i]);
        }

        reindex();

        return true;
    }

    public boolean removeStopWord(String stopWord){

        if (stopWord.equals(null))
            return false;
        if (stopWords.contains(stopWord))
            stopWords.remove(stopWord);

        reindex();

        return true;
    }

    public List<String> getStopWords() {
        return stopWords;
    }

    /**
     * Agrega las stop words desde el archivo: /resources/indexFiles/stopWords.txt
     * en un arreglo de strings.
     */
    public void addStopWords(){
        String sw = "";
        String arrelgo[];
        sw = loadFile.getFile(STOP_WORDS_FILE);
        arrelgo = sw.toLowerCase().split(" ");

        for (int i = 0; i < arrelgo.length; i++){
            stopWords.add(arrelgo[i]);
        }
    }

    /**
     * Método que regresa una Lista con las keywords de un archivo.
     * @param data es el archivo ya limpio de tags html
     * @return
     */
    public LinkedList<String> getKeyWords(String data){
        String arrelgoPalabras[];
        LinkedList<String> linkedList = new LinkedList<String>();
        arrelgoPalabras = data.replaceAll("[^a-zA-Z'\\s]","").toLowerCase().split("\\s+");

        for (int i = 0; i < arrelgoPalabras.length; i++){
            if (stopWords.contains(arrelgoPalabras[i]))
                continue;
            linkedList.add(arrelgoPalabras[i]);
        }
        return linkedList;
    }

    /**
     * Método que evalua una expresión.
     * @param expr
     * @return Arreglo con los archivos
     */
    public LinkedList<String> evaluaExp(String expr){
        ArrayList<String> doctos = new ArrayList<String>();
        LinkedList<String> elem1 = new LinkedList<String>();
        LinkedList<String> elem2 = new LinkedList<String>();
        ArrayList<String> operadores = new ArrayList<String>();
        operadores.add("-");
        operadores.add("&");
        operadores.add("||");
        operadores.add("|");
        operadores.add(">");
        operadores.add(")");
        operadores.add("(");

        Stack<String> E = conviertePosfija(expr);
        String popRes;
        Stack<LinkedList<String>> S = new Stack<LinkedList<String>>(); //Pila salida

        while (!E.isEmpty()){
            popRes = E.pop();
            if (operadores.contains(popRes)){

                if ( S.isEmpty() ) {
                    return new LinkedList<>();
                }

                elem2 = S.pop();
                if ( ! popRes.equals("-") ) {
                    if ( S.isEmpty() ) {
                        return new LinkedList<>();
                    }

                    elem1 = S.pop();
                }
            } else {
                LinkedList<String> list = index.get(popRes);
                if ( list == null ) {
                    S.push(new LinkedList<>());
                } else {
                    S.push(list);
                }
            }
            if (popRes.equals("&")){
                S.push(operationAND(elem1, elem2));
            }
            if (popRes.equals("|")){
                S.push(operationOR(elem1, elem2));
            }
            if (popRes.equals("-")){
                S.push(operationNOT(elem2));
            }
            if (popRes.equals(">")){
                S.push(operationIMPLIES(elem1, elem2));
            }
            if (popRes.equals("||")){
                S.push(operationXOR(elem1, elem2));
            }

        }

        if ( S.isEmpty() ) {
            return new LinkedList();
        }

        return S.pop();
    }


    public LinkedList<String> operationAND(LinkedList<String> elem1, LinkedList<String> elem2){
        LinkedList<String> elem = (LinkedList) elem1.clone();
         elem.retainAll(elem2);
        return elem;
    }
    public LinkedList<String> operationOR(LinkedList<String> elem1, LinkedList<String> elem2) {
        LinkedList<String> elem = (LinkedList) elem1.clone();
        LinkedList elem2c = (LinkedList)elem2.clone();
        elem2c.removeAll(elem1);
        elem.addAll(elem2c);
        return elem;
    }
    private LinkedList<String> operationNOT(LinkedList<String> elem1) {
        LinkedList<String> t = new LinkedList<String>();

        Set<Map.Entry<String, LinkedList<String>>> entrySet = index.entrySet();
        for (Map.Entry entry : entrySet) {
            t.removeAll((LinkedList)entry.getValue());
            t.addAll((LinkedList)entry.getValue());
        }
        t.removeAll(elem1);
        return t;
    }
    private LinkedList<String> operationIMPLIES(LinkedList<String> elem1, LinkedList<String> elem2) {
        LinkedList<String> elem = (LinkedList) elem1.clone();
        LinkedList<String> t = new LinkedList<String>();

        for (String palabra : elem){
            if (!elem2.contains(palabra)){
                continue;
            }
            t.add(palabra);
        }
        return t;
    }
    private LinkedList<String> operationXOR(LinkedList<String> elem1, LinkedList<String> elem2) {
        LinkedList<String> elem = (LinkedList) elem1.clone();
        LinkedList<String> t = new LinkedList<String>();

        for (String palabra : elem){
            if (!elem2.contains(palabra)){
                t.add(palabra);
            }
        }
        for (String palabra : elem2){
            if (!elem1.contains(palabra)){
                t.add(palabra);
            }
        }
        return t;
    }


    /**
     * Método que convierte de infija a posfija una expresión
     * @param expresion expresión booleana a convertir
     * @return expresión en posfijo
     */
    public Stack<String> conviertePosfija(String expresion) {
        Scanner leer = new Scanner(System.in);
        String expr = depurar(expresion);
        String[] arrayInfix = expr.split(" ");

        //Declaración de las pilas
        Stack<String> E = new Stack<String>(); //Pila entrada
        Stack<String> P = new Stack<String>(); //Pila temporal para operadores
        Stack<String> S = new Stack<String>(); //Pila salida
        Stack<String> SR = new Stack<String>(); //Pila salida real

        //Añadir la array a la Pila de entrada (E)
        for (int i = arrayInfix.length - 1; i >= 0; i--) {
            E.push(arrayInfix[i]);
        }

        try {
            //Algoritmo Infijo a Postfijo
            while (!E.isEmpty()) {
                switch (pref(E.peek())) {
                    case 1:
                        P.push(E.pop());
                        break;
                    case 3:
                    case 5:
                    case 6:
                    case 4:
                    case 7:
                        while (pref(P.peek()) >= pref(E.peek())) {
                            S.push(P.pop());
                        }
                        P.push(E.pop());
                        break;
                    case 2:
                        while (!P.peek().equals("(")) {
                            S.push(P.pop());
                        }
                        P.pop();
                        E.pop();
                        break;
                    default:
                        S.push(E.pop());
                }
            }

        } catch (Exception e){

        }

        while (!S.isEmpty()){
              SR.push(S.pop());
        }

        return SR;
    }
    /**
     * Depurar expresión: Elimina espacios en blanco y agrega parentesis a la expresión
     */
    private String depurar(String s) {
        String s1 = addAnd(s.toLowerCase());
        s = s1.replaceAll("\\s+", ""); //Elimina espacios en blanco
        s = "(" + s + ")";
        String simbols = "&|->||()";
        String str = "";

        //Deja espacios entre operadores
        for (int i = 0; i < s.length(); i++) {
            if (simbols.contains("" + s.charAt(i))) {
                if (s.charAt(i) == '|' && s.charAt(i+1) == '|'){
                    str += " " + s.charAt(i) + s.charAt(i+1) + " ";
                    i++;
                }
                else
                    str += " " + s.charAt(i) + " ";
            }else str += s.charAt(i);
        }
        return str.replaceAll("\\s+", " ").trim();
    }

    private String addAnd(String expr) {
        String nexpr = expr;
        do {
            expr = nexpr;
            nexpr = expr.replaceAll("([a-z0-9]+)(\\s+)([a-z0-9]+)", "$1 & $3");
        } while ( ! nexpr.equals( expr ) );
        return nexpr;
    }


    /**
     * Jerarquia de los operadores
     * @param op
     * @return
     */
    private int pref(String op) {
        int prf = 99;
        if (op.equals("-")) prf = 7;
        if (op.equals("&")) prf = 6;
        if (op.equals("||")) prf = 5;
        if (op.equals("|")) prf = 4;
        if (op.equals(">")) prf = 3;
        if (op.equals(")")) prf = 2;
        if (op.equals("(")) prf = 1;
        return prf;
    }

}
